package com.matm.matmsdk.aepsmodule;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryAEPS2RequestModel;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryContract;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryPresenter;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryResponse;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameListActivity;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameModel;
import com.matm.matmsdk.aepsmodule.cashwithdrawal.AepsResponse;
import com.matm.matmsdk.aepsmodule.cashwithdrawal.CashWithDrawalContract;
import com.matm.matmsdk.aepsmodule.cashwithdrawal.CashWithdrawalAEPS2RequestModel;
import com.matm.matmsdk.aepsmodule.cashwithdrawal.CashWithdrawalPresenter;
import com.matm.matmsdk.aepsmodule.cashwithdrawal.CashWithdrawalResponse;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.DeviceInfo;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.MorphoDeviceInfo;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.MorphoPidData;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.Opts;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.PidData;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.PidOptions;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.AuthReq;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.AuthRes;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.Meta;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.Uses;
import com.matm.matmsdk.aepsmodule.maskedittext.MaskedEditText;
import com.matm.matmsdk.aepsmodule.ministatement.StatementResponse;
import com.matm.matmsdk.aepsmodule.ministatement.StatementTransactionActivity;
import com.matm.matmsdk.aepsmodule.signer.XMLSigner;
import com.matm.matmsdk.aepsmodule.transactionstatus.TransactionStatusAeps2Activity;
import com.matm.matmsdk.aepsmodule.transactionstatus.TransactionStatusModel;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Constants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;
import com.moos.library.HorizontalProgressView;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import isumatm.androidsdk.equitas.R;

public class AEP2HomeActivity extends AppCompatActivity implements BalanceEnquiryContract.View, CashWithDrawalContract.View {

    private PidData pidData;
    private MorphoPidData morphoPidData;
    private Serializer serializer;
    private ArrayList<String> positions;
    String tag = "";
    private UsbDevice usbDevice;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid="SAGEM SA";
    String mantradeviceid="MANTRA";
    String morphoe2device="Morpho";
    UsbManager musbManager;
    LinearLayout tabLayout;
    ImageView two_fact_fingerprint ;
    ProgressBar two_fact_depositBar;
    Button two_fact_submitButton;
    Dialog deleteDialog;
    private EditText aadharNumber,aadharVirtualID;
    private TextView balanceEnquiryExpandButton, cashWithdrawalButton, fingerprintStrengthDeposit, depositNote;
    private EditText mobileNumber,bankspinner,amountEnter;
    private ImageView fingerprint,virtualID,aadhaar;
    private HorizontalProgressView depositBar;
    private Button submitButton;

    private BalanceEnquiryPresenter balanceEnquiryPresenter;
    private CashWithdrawalPresenter cashWithdrawalPresenter;

    private boolean isStartDate = false;

    Session session;


    //BalanceEnquiryRequestModel balanceEnquiryRequestModel;
    //CashWithdrawalRequestModel cashWithdrawalRequestModel;

    BalanceEnquiryAEPS2RequestModel balanceEnquiryaeps2RequestModel;
    CashWithdrawalAEPS2RequestModel cashWithdrawalaeps2RequestModel;

    String bankIINNumber = "";
    ArrayList<String> featurescodesresponse;


    ProgressDialog loadingView;

    String aadharCardValidation="";
    String balanceaadharNo = "";
    TextView virtualidText,aadharText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_aeps_home );
        serializer = new Persister();
        session = new Session(AEP2HomeActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if(AepsSdkConstants.applicationType.equalsIgnoreCase("CORE")){
            session.setUserToken(AepsSdkConstants.tokenFromCoreApp);
            session.setUsername(AepsSdkConstants.userNameFromCoreApp);

        }else {

            if (AepsSdkConstants.encryptedData.trim().length() != 0 && AepsSdkConstants.paramA.trim().length() != 0 && AepsSdkConstants.paramB.trim().length() != 0 && AepsSdkConstants.transactionType.trim().length() != 0 && AepsSdkConstants.loginID.trim().length() != 0) {

                getUserAuthToken();
            } else {
                showAlert("Request parameters are missing. Please check and try again..");
            }
        }



        fingerprintStrengthDeposit = findViewById ( R.id. fingerprintStrengthDeposit );
        depositNote = findViewById ( R.id. depositNote );
        depositNote.setVisibility ( View.GONE );
        fingerprintStrengthDeposit.setVisibility ( View.GONE );
        aadharVirtualID = (EditText) findViewById(R.id.aadharVirtualID);
        virtualID = findViewById(R.id.virtualID);
        aadhaar = findViewById(R.id.aadhaar);
        virtualidText = findViewById(R.id.virtualidText);
        aadharText = findViewById(R.id.aadharText);
        tabLayout = findViewById ( R.id. tabLayout );
        aadharNumber = findViewById ( R.id. aadharNumber );
        mobileNumber = findViewById ( R.id. mobileNumber );
        bankspinner = findViewById ( R.id. bankspinner );
        amountEnter = findViewById ( R.id. amountEnter );
        fingerprint = findViewById ( R.id. fingerprint );
        submitButton = findViewById ( R.id. submitButton );
        depositBar = findViewById ( R.id. depositBar );
        depositBar.setVisibility ( View.GONE );

        cashWithdrawalButton = findViewById ( R.id. cashWithdrawalButton );

        balanceEnquiryExpandButton = findViewById ( R.id. balanceEnquiryExpandButton );

        musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
        updateDeviceList ();


        positions = new ArrayList<>();
        positions = new ArrayList<>();

        if(AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)){
            balanceEnquiryExpandButton.setVisibility(View.VISIBLE);
            cashWithdrawalButton.setVisibility(View.GONE);
            amountEnter.setVisibility(View.GONE);

        }else if(AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)){
            balanceEnquiryExpandButton.setVisibility(View.VISIBLE);
            balanceEnquiryExpandButton.setText("Mini Statement");
            cashWithdrawalButton.setVisibility(View.GONE);
            amountEnter.setVisibility(View.GONE);
        }else if(AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)){

            balanceEnquiryExpandButton.setVisibility(View.GONE);
            cashWithdrawalButton.setVisibility(View.VISIBLE);
            amountEnter.setText(AepsSdkConstants.transactionAmount);
            amountEnter.setEnabled(false);

        }




        bankspinner.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                Intent in = new Intent(AEP2HomeActivity.this, BankNameListActivity.class);
                if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                    startActivityForResult(in, AepsSdkConstants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE);

                } else if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                    startActivityForResult(in, AepsSdkConstants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE);

                }else if(AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)){
                    startActivityForResult(in, AepsSdkConstants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE);
                }

            }
        } );




        fingerprint.setOnClickListener ( new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                showLoader ();
                fingerprint.setEnabled(false);
                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner_grey));
                /*registerReceiver(mUsbDeviceReceiver, new IntentFilter ( UsbManager.ACTION_USB_DEVICE_DETACHED));
                registerReceiver(mUsbDeviceReceiver, new IntentFilter( UsbManager.ACTION_USB_DEVICE_ATTACHED));*/
                musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                updateDeviceList ();
                if(usbDevice !=null) {
                    if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( mantradeviceid )) {
                        capture ();
                    } else if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphodeviceid )||usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphoe2device ) ) {
                        // Toast.makeText ( DashboardActivity.this, "devicemorpho"+usbDevice.getManufacturerName (), Toast.LENGTH_SHORT ).show ();
                        morophoCapture ();
                    }
                }else{
                    musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                    updateDeviceList ();
//                    deviceConnectMessgae ();
                }

            }
        } );



        aadharNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String initial = s.toString();
                // remove all non-digits characters
                String processed = initial.replaceAll("\\D", " ");

                // insert a space after all groups of 4 digits that are followed by another digit
                processed = processed.replaceAll("(\\d{4})(?=\\d)(?=\\d)(?=\\d)", "$1 ");

                //Remove the listener
                aadharNumber.removeTextChangedListener(this);

                int index = aadharNumber.getSelectionEnd();

                if (index == 5 || index == 10)
                    if (count > before)
                        index++;
                    else
                        index--;

                //Assign processed text
                aadharNumber.setText(processed);

                try {
                    aadharNumber.setSelection(index);
                } catch (Exception e) {
                    e.printStackTrace();
                    aadharNumber.setSelection(s.length() - 1);
                }
                //Give back the listener
                aadharNumber.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        aadharVirtualID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String initial = s.toString();
                // remove all non-digits characters
                String processed = initial.replaceAll("\\D", " ");

                // insert a space after all groups of 4 digits that are followed by another digit
                processed = processed.replaceAll("(\\d{4})(?=\\d)(?=\\d)(?=\\d)", "$1 ");

                //Remove the listener
                aadharVirtualID.removeTextChangedListener(this);

                int index = aadharVirtualID.getSelectionEnd();

                if (index == 5 || index == 10 || index == 15)
                    if (count > before)
                        index++;
                    else
                        index--;

                //Assign processed text
                aadharVirtualID.setText(processed);

                try {
                    aadharVirtualID.setSelection(index);
                } catch (Exception e) {
                    e.printStackTrace();
                    aadharVirtualID.setSelection(s.length() - 1);
                }
                //Give back the listener
                aadharVirtualID.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        bankspinner.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    bankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                }
                if (s.length () > 0) {
                    bankspinner.setError ( null );
                }
            }
        } );




        amountEnter.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    amountEnter.setError ( getResources ().getString ( R.string.amount_error ) );
                }
                if (s.length () > 0) {
                    amountEnter.setError ( null );
                }
            }
        } );



        mobileNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 10) {
                    mobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                }
                if (s.length () > 0) {
                    mobileNumber.setError ( null );
                    String x = s.toString ();
                    if (x.startsWith ( "0" )|| Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == false) {
                        mobileNumber.setError ( getResources ().getString ( R.string.mobilevaliderror ) );
                    }
                }
            }
        } );


       /* virtualID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//* Will implement when VID will used*//*

                aadharNumber.setVisibility(View.GONE);
                aadharVirtualID.setVisibility(View.VISIBLE);
                virtualID.setEnabled(false);
                aadhaar.setEnabled(true);
                virtualID.setBackgroundResource(R.drawable.ic_language_blue);
                virtualidText.setTextColor(getResources().getColor(R.color.light_blue));
                aadhaar.setBackground(getResources().getDrawable(R.drawable.ic_fingerprint_grey));
                aadharText.setTextColor(getResources().getColor(R.color.grey));


            }
        });

        aadhaar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aadharNumber.setVisibility(View.VISIBLE);
                aadharVirtualID.setVisibility(View.GONE );
                virtualID.setEnabled(true);
                aadhaar.setEnabled(false);
                virtualID.setBackgroundResource(R.drawable.ic_language);
                virtualidText.setTextColor(getResources().getColor(R.color.grey));

                aadhaar.setBackgroundResource(R.drawable.ic_fingerprint_blue);
                aadharText.setTextColor(getResources().getColor(R.color.light_blue));
            }
        });
*/

        submitButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                String balanceaadharNo = "";
                //if(bl_aadhar_no_rd.isChecked()) {
                    balanceaadharNo = aadharNumber.getText().toString();
                    if (balanceaadharNo.contains(" ")) {
                        balanceaadharNo = balanceaadharNo.replaceAll(" ", "").trim();
                    }
                if (balanceaadharNo == null || balanceaadharNo.matches("")) {
                    aadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                    return;
                }
               // }



                if (mobileNumber.getText () == null || mobileNumber.getText ().toString ().trim ().matches ( "" )|| Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == false) {
                    mobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                    return;
                }else{
                    String panaaadhaar = mobileNumber.getText ().toString ().trim ();
                    if ( !panaaadhaar.contains (" ") && panaaadhaar.length () == 10  ){
                    }else{
                        mobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                        return;
                    }
                }
                if(bankspinner.getText ()==null|| bankspinner.getText ().toString ().trim().matches ( "" )){
                    bankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                    return;
                }
                if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {

                } else if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)){

                }else {
                    if (amountEnter.getText() == null || amountEnter.getText().toString().trim().matches("")) {
                        amountEnter.setError(getResources().getString(R.string.amount_error));
                        return;
                    }


                }
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                    }
                    if (!pidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                            tag = "3";

                        } else if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                            tag = "1";

                        }else if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                            tag = "3";

                        }
                        new AuthRequest( balanceaadharNo, pidData ).executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR );
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                    }
                    if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                            tag = "3";

                        } else if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                            tag = "1";

                        }else if (AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                            tag = "3";

                        }
                        new AuthRequestMorpho( balanceaadharNo, morphoPidData ).executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR );
                    }
                }
            }
        } );





    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void morphoMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void mantraMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void rdserviceMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private  void installcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if(isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled){
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage ( "com.mantra.rdservice" );
                startActivityForResult ( intent, 1 );
            }else{
                rdserviceMessage ();

            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage ();
        }
    }
    private  void morphoinstallcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
        if(isAppInstalled) {
//This intent will help you to launch if the package is already installed
            Intent intent1 = new Intent();
            intent1.setAction ( "in.gov.uidai.rdservice.fp.INFO" );
            intent1.setPackage ( "com.scl.rdservice" );
//            intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivityForResult ( intent1, 3 );
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            morphoMessage ();
        }
    }

    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy ();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if(usbDevice == null) {
            deviceConnectMessgae ();
        }else {
            if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( mantradeviceid )) {
                installcheck ();
            } else if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphodeviceid )||usbDevice.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )) {
                morphoinstallcheck ();
            }
        }
    }
    private void deviceConnectMessgae (){
        hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(AEP2HomeActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.setting_device))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        hideLoader ();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            // Toast.makeText(DashboardActivity.this, "No Devices Currently Connected" + usbconnted, Toast.LENGTH_LONG).show();
            deviceConnectMessgae ();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        if(device.getManufacturerName ().equalsIgnoreCase ( mantradeviceid )||device.getManufacturerName ().equalsIgnoreCase ( morphodeviceid )||device.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )){
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName();
                        }
                    }
                }
            }
            devicecheck ();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AepsSdkConstants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(AepsSdkConstants.IIN_KEY);
                bankspinner.setText(bankIINValue.getBankName());
                bankIINNumber = bankIINValue.getIin();
                checkBalanceEnquiryValidation();
            }

            checkBalanceEnquiryValidation();

        }else if (requestCode == AepsSdkConstants.REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(AepsSdkConstants.IIN_KEY);
                bankspinner.setText(bankIINValue.getBankName());
                bankIINNumber = bankIINValue.getIin();
                checkCashDepositValidation();
            }
            checkCashDepositValidation();
        }
        else if (requestCode == AepsSdkConstants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(AepsSdkConstants.IIN_KEY);
                bankspinner.setText(bankIINValue.getBankName());
                bankIINNumber = bankIINValue.getIin();
                checkWithdrawalValidation();

            }
            checkWithdrawalValidation();
        }

        else if (requestCode == AepsSdkConstants.REQUEST_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                Intent respIntent = new Intent();
                respIntent.putExtra(AepsSdkConstants.responseData, AepsSdkConstants.transactionResponse);
                setResult(Activity.RESULT_OK,respIntent);
                finish();

            }
            checkWithdrawalValidation();
        }
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "DEVICE_INFO" );
                            String rdService = data.getStringExtra ( "RD_SERVICE_INFO" );
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.dismiss();
                        }
                    }
                    if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                        checkBalanceEnquiryValidation();
                    } else if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                        checkWithdrawalValidation();
                    }else  if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                        checkBalanceEnquiryValidation();
                    }
                }
                break;
            case 2:
                if(loadingView!=null){
                    loadingView.dismiss();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            if (result != null) {
                                pidData = serializer.read ( PidData.class, result );

                                if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                    if(!AepsSdkConstants.bioauth) {
                                        fingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                                        depositBar.setVisibility(View.VISIBLE);
                                        depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        depositBar.setProgressTextMoved(true);
                                        depositBar.setEndColor(getResources().getColor(R.color.red));
                                        depositBar.setStartColor(getResources().getColor(R.color.red));
                                        depositNote.setVisibility(View.VISIBLE);

                                    }
                                    if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(0);
                                       }
                                }else if(Float.parseFloat ( pidData._Resp.qScore ) >=60 && Float.parseFloat ( pidData._Resp.qScore ) <=70){
                                    if(!AepsSdkConstants.bioauth) {

                                        depositBar.setVisibility(View.VISIBLE);
                                        depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        depositBar.setProgressTextMoved(true);
                                        depositBar.setEndColor(getResources().getColor(R.color.yellow));
                                        depositBar.setStartColor(getResources().getColor(R.color.yellow));
                                        depositNote.setVisibility(View.VISIBLE);
                                        fingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                                    }
                                    if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(70);
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }
                                }else{
                                    if(!AepsSdkConstants.bioauth) {


                                        depositBar.setVisibility(View.VISIBLE);
                                        depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        depositBar.setProgressTextMoved(true);
                                        depositBar.setEndColor(getResources().getColor(R.color.green));
                                        depositBar.setStartColor(getResources().getColor(R.color.green));
                                        depositNote.setVisibility(View.VISIBLE);
                                        fingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                                    }
                                    if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress((int)Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }
                                }
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.device_is_not_ready_error));
                            }
                            if (pidData._Resp.errCode.equalsIgnoreCase("720")){
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo );
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "0" ) && Float.parseFloat ( pidData._Resp.qScore )>=60){
                                fingerprint.setEnabled ( false );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner_grey));
                                Util.showAlert ( AEP2HomeActivity.this,getResources ().getString ( R.string.success ),getResources ().getString ( R.string.capture_success ) );
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo + " " + ". Please Try Again !!!");
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),"Capture stopped or Aborted. Please Try Again !!!");
                            }else if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo+ " " +"with a score less then 60% " + ". Please Try Again !!!");
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo+ " " + ". Please Try Again !!!");
                            }

                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.dismiss();
                        }
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        depositBar.setVisibility ( View.GONE );
                        depositNote.setVisibility ( View.GONE );
                        fingerprintStrengthDeposit.setVisibility ( View.GONE );
                        Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),getResources().getString(R.string.scanning_error));
                    }
                    if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                        checkBalanceEnquiryValidation();
                    } else if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                        checkWithdrawalValidation();
                    }else  if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                        checkBalanceEnquiryValidation();
                    }
                }
                break;

            case 3:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "DEVICE_INFO" );
                            String rdService = data.getStringExtra ( "RD_SERVICE_INFO" );
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.dismiss();
                        }
                    }
                    if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                        checkBalanceEnquiryValidation();
                    } else if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                        checkWithdrawalValidation();
                    }else  if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                        checkBalanceEnquiryValidation();
                    }
                }
                break;
            case 4:
                if(loadingView!=null){
                    loadingView.dismiss();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            if (result != null) {
                                morphoPidData = serializer.read ( MorphoPidData.class, result );

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.device_is_not_ready_error));
                            }
                            if (morphoPidData._Resp.errCode.equalsIgnoreCase("720")){
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo );
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "0" )){
                                if(!AepsSdkConstants.bioauth) {
                                    fingerprint.setEnabled(false);
                                    fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner_grey));
                                    depositBar.setVisibility(View.GONE);
                                    depositNote.setVisibility(View.GONE);
                                    fingerprintStrengthDeposit.setVisibility(View.GONE);
                                    Util.showAlert(AEP2HomeActivity.this, getResources().getString(R.string.success), getResources().getString(R.string.capture_success));
                                }
                                if(two_fact_depositBar!=null) {
                                    two_fact_depositBar.setProgress((int)Float.parseFloat(pidData._Resp.qScore));
                                   two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                    two_fact_submitButton.setEnabled(true);
                                    two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                }
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo + " " + ". Please Try Again !!!");
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),"Capture stopped or Aborted. Please Try Again !!!");
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo+ " " + ". Please Try Again !!!");

                            }
                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.dismiss();
                        }
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        depositBar.setVisibility ( View.GONE );
                        depositNote.setVisibility ( View.GONE );
                        fingerprintStrengthDeposit.setVisibility ( View.GONE );
                        Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.fail_error),getResources().getString(R.string.scanning_error));

                    }
                    if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                        checkBalanceEnquiryValidation();
                    } else if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                        checkWithdrawalValidation();
                    }else  if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                        checkBalanceEnquiryValidation();
                    }
                }
                break;
        }
    }


    @Override
    public void checkBalanceEnquiryStatus(String status, String message, BalanceEnquiryResponse balanceEnquiryResponse) {
        String aadhar = aadharNumber.getText().toString().trim();
        releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (balanceEnquiryResponse!=null){
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(balanceEnquiryResponse.getBankName());
            transactionStatusModel.setBalanceAmount(balanceEnquiryResponse.getBalance());
            transactionStatusModel.setReferenceNo(balanceEnquiryResponse.getReferenceNo());
            transactionStatusModel.setTransactionType("Balance Enquiry");
            transactionStatusModel.setStatus (balanceEnquiryResponse.getStatus ());
            transactionStatusModel.setApiComment (balanceEnquiryResponse.getApiComment ());
            transactionStatusModel.setStatusDesc (balanceEnquiryResponse.getStatusDesc ());
            session.setFreshnessFactor ( balanceEnquiryResponse.getNextFreshnessFactor () );

            Gson g = new Gson();
            String jsonString = g.toJson(transactionStatusModel);
            AepsSdkConstants.transactionResponse = jsonString;//transactionStatusModel.toString().replace("TransactionStatusModel","");
            Intent intent = new Intent(AEP2HomeActivity.this, TransactionStatusAeps2Activity.class);
            intent.putExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY,transactionStatusModel);
            //startActivityForResult (intent, AepsSdkConstants.BALANCE_RELOAD);
            startActivityForResult (intent, AepsSdkConstants.REQUEST_CODE);
        }else{
            transactionStatusModel = null;
            session.setFreshnessFactor ( null );
            session.clear();
            showAlert("Unauthorized, Session Expired ");
        }

    }

    @Override
    public void checkBalanceEnquiryAEPS2(String status, String message, AepsResponse balanceEnquiryResponse) {


        String aadhar = aadharNumber.getText().toString().trim();
        releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (balanceEnquiryResponse!=null){
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(balanceEnquiryResponse.getBankName());
            transactionStatusModel.setBalanceAmount(balanceEnquiryResponse.getBalance());
            transactionStatusModel.setReferenceNo(balanceEnquiryResponse.getRrn());
            transactionStatusModel.setTransactionType("Balance Enquery");
            transactionStatusModel.setStatus (balanceEnquiryResponse.getStatus ());
            transactionStatusModel.setApiComment (balanceEnquiryResponse.getApiComment ());
            transactionStatusModel.setStatusDesc (balanceEnquiryResponse.getTransactionStatus());
            session.setFreshnessFactor ("");

            Gson g = new Gson();
            String jsonString = g.toJson(transactionStatusModel);
            AepsSdkConstants.transactionResponse = jsonString;//transactionStatusModel.toString().replace("TransactionStatusModel","");
            Intent intent = new Intent(AEP2HomeActivity.this, TransactionStatusAeps2Activity.class);
            intent.putExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY,transactionStatusModel);
            //startActivityForResult (intent, AepsSdkConstants.BALANCE_RELOAD);
            startActivityForResult (intent, AepsSdkConstants.REQUEST_CODE);
        }else{
            transactionStatusModel = null;
            session.setFreshnessFactor ( null );
            session.clear();
            showAlert("Unauthorized, Session Expired ");
        }
    }

    @Override
    public void checkStatementEnquiryAEPS2(String status, String message, StatementResponse statementResponse) {
        String aadhar = aadharNumber.getText().toString().trim();
        releaseData ();
        if (statementResponse!=null){
            session.setFreshnessFactor ("");
            Gson g = new Gson();
            JsonReader reader = new JsonReader(new StringReader(statementResponse.toString()));
            reader.setLenient(true);

            String jsonString = g.toJson(statementResponse);
            AepsSdkConstants.transactionResponse = jsonString;//transactionStatusModel.toString().replace("TransactionStatusModel","");
            Intent intent = new Intent(AEP2HomeActivity.this, StatementTransactionActivity.class);
            intent.putExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY,jsonString);
            //startActivityForResult (intent, AepsSdkConstants.BALANCE_RELOAD);
            startActivityForResult (intent, AepsSdkConstants.REQUEST_CODE);
        }else{
            session.setFreshnessFactor ( null );
            session.clear();
            showAlert("Unauthorized, Session Expired, Please try agai later. ");
        }

    }

    @Override
    public void checkCashWithdrawalStatus(String status, String message, CashWithdrawalResponse cashWithdrawalResponse) {
        String aadhar = aadharNumber.getText().toString().trim();
        String amount = amountEnter.getText ().toString ().trim ();
        releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (cashWithdrawalResponse!=null){
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(cashWithdrawalResponse.getBankName());
            transactionStatusModel.setBalanceAmount(cashWithdrawalResponse.getBalance());
            transactionStatusModel.setReferenceNo(cashWithdrawalResponse.getReferenceNo());
            transactionStatusModel.setTransactionAmount(amount);
            transactionStatusModel.setTransactionType("Cash Withdrawal");
            transactionStatusModel.setStatus (cashWithdrawalResponse.getStatus ());
            transactionStatusModel.setApiComment (cashWithdrawalResponse.getApiComment ());
            transactionStatusModel.setStatusDesc (cashWithdrawalResponse.getStatusDesc ());
            session.setFreshnessFactor ( cashWithdrawalResponse.getNextFreshnessFactor () );

            Gson g = new Gson();
            String jsonString = g.toJson(transactionStatusModel);
            AepsSdkConstants.transactionResponse = jsonString;//transactionStatusModel.toString().replace("TransactionStatusModel","");
            Intent intent = new Intent(AEP2HomeActivity.this, TransactionStatusAeps2Activity.class);
            intent.putExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY,transactionStatusModel);
            //startActivityForResult (intent, AepsSdkConstants.BALANCE_RELOAD);
            startActivityForResult (intent, AepsSdkConstants.REQUEST_CODE);
        }else{
            transactionStatusModel = null;
            session.setFreshnessFactor ( null );
            session.clear();
            showAlert("Unauthorized, Session Expired ");
        }

    }

    @Override
    public void checkCashWithdrawalAEPS2(String status, String message, AepsResponse cashWithdrawalResponse) {
        // Toast.makeText(this, "here new", Toast.LENGTH_SHORT).show();
        String aadhar = aadharNumber.getText().toString().trim();
        String amount = amountEnter.getText ().toString ().trim ();
        releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (cashWithdrawalResponse!=null){
            transactionStatusModel.setAadharCard(cashWithdrawalResponse.getCustomerAadhaarNo());
            transactionStatusModel.setBankName(cashWithdrawalResponse.getBankName());
            transactionStatusModel.setBalanceAmount(cashWithdrawalResponse.getBalance());
            transactionStatusModel.setReferenceNo(cashWithdrawalResponse.getRrn());
            transactionStatusModel.setTransactionAmount(amount);
            transactionStatusModel.setTransactionType("Cash Withdrawal");
            transactionStatusModel.setStatus (cashWithdrawalResponse.getStatus ());
            transactionStatusModel.setApiComment (cashWithdrawalResponse.getApiComment ());
            transactionStatusModel.setStatusDesc (message);
            session.setFreshnessFactor ( "" );
        }else{
            transactionStatusModel = null;
            session.setFreshnessFactor ( null );
        }
        Intent intent = new Intent(AEP2HomeActivity.this, TransactionStatusAeps2Activity.class);
        intent.putExtra(Constants.TRANSACTION_STATUS_KEY,transactionStatusModel);
        startActivityForResult (intent, AepsSdkConstants.REQUEST_CODE);
    }


    @Override
    public void checkEmptyFields() {
        Toast.makeText(AEP2HomeActivity.this, "Kindly get Registered with AEPS to proceed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = new ProgressDialog(AEP2HomeActivity.this);
            loadingView.setCancelable(false);
            loadingView.setMessage("Please Wait..");
        }
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.dismiss();
        }
    }

    /*
     * Biomectirc device's capture data
     */
    private void capture(){
        try {
            String pidOption = getPIDOptions ();
            if (pidOption != null) {
                Intent intent2 = new Intent();
                intent2.setAction ( "in.gov.uidai.rdservice.fp.CAPTURE" );
                intent2.setPackage ( "com.mantra.rdservice" );
                intent2.putExtra ( "PID_OPTIONS", pidOption );
                startActivityForResult ( intent2, 2 );
            }
        } catch (Exception e) {
            if(loadingView!=null){
                loadingView.dismiss();
            }
            if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                checkBalanceEnquiryValidation();
            } else if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                checkWithdrawalValidation();
            }else  if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                checkBalanceEnquiryValidation();
            }
            fingerprint.setEnabled ( true );
            fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

            depositBar.setVisibility ( View.GONE );
            depositNote.setVisibility ( View.GONE );
            Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.setting_device));
        }
    }

    /*
     * Biomectirc device's capture data
     */
    private void morophoCapture(){
        try {
            String pidOption = getPIDOptions ();
            if (pidOption != null) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.CAPTURE");
                intent.setPackage("com.scl.rdservice");
                intent.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent, 4);
            }
        } catch (Exception e) {

            if(loadingView!=null){
                loadingView.dismiss();
            }
            if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.balanceEnquiry)) {
                checkBalanceEnquiryValidation();
            } else if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)) {
                checkWithdrawalValidation();
            }else  if (mobileNumber.getText() != null && !mobileNumber.getText().toString().matches("") && AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.ministatement)) {
                checkBalanceEnquiryValidation();
            }
            fingerprint.setEnabled ( true );
            fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

            depositBar.setVisibility ( View.GONE );
            depositNote.setVisibility ( View.GONE );
            Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.setting_device));

        }

    }

    /*
     * data needed for the biometric device's
     *
     * for device info and the capture of the finger prints
     */
    private String getPIDOptions() {
        try {
            String posh = getResources ().getString ( R.string.posh );
            if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }

            Opts opts = new Opts();
            opts.fCount = "1";
            opts.fType = "0";
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format ="0";
            opts.pidVer = "2.0";
            opts.timeout = "10000";
            opts.posh = posh;
            opts.env = "P";

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
        }
        return null;
    }

    private class AuthRequest extends AsyncTask<Void, Void, String> {

        private String uid;
        private PidData pidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info ;

        private AuthRequest(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
            dialog = new ProgressDialog(AEP2HomeActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = session.getFreshnessFactor();

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq!=null && meta!=null && info!=null) {


                if (tag.equalsIgnoreCase("3")) {
                    String vid = null;
                    String uid = null;

                        uid = aadharNumber.getText().toString();
                    if(aadhaar.isEnabled()){
                        if (uid.contains(" ")) {
                            uid = uid.replaceAll(" ", "").trim();
                        }
                    }else if(virtualID.isEnabled()){
                        vid = aadharVirtualID.getText().toString().trim();
                        if (vid.contains(" ")) {
                            vid = vid.replaceAll(" ", "").trim();
                        }
                    }



                    String value = authReq.skey.value.toString();

                    //  String last = value.charAt((value.length()) -1);
                    String last = String.valueOf(value.charAt(value.length()-1));
                    if(last.equalsIgnoreCase("\n")){
                        value = value.replace("\n","");
                    }
                    balanceEnquiryaeps2RequestModel = new BalanceEnquiryAEPS2RequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, value,"WEBUSER",session.getUserName());
                    balanceEnquiryPresenter = new BalanceEnquiryPresenter(AEP2HomeActivity.this);
                    checkVPNstatusForTransaction("Balance");


                }
                else if (tag.equalsIgnoreCase("1")) {
                    String vid = null;
                    String uid = null;
                        uid = aadharNumber.getText().toString();
                    if(aadhaar.isEnabled()){
                        if (uid.contains(" ")) {
                            uid = uid.replaceAll(" ", "").trim();
                        }
                    }else if(virtualID.isEnabled()){
                        vid = aadharVirtualID.getText().toString().trim();
                        if (vid.contains(" ")) {
                            vid = vid.replaceAll(" ", "").trim();
                        }
                    }


                    String value = authReq.skey.value.toString();

                    //  String last = value.charAt((value.length()) -1);
                    String last = String.valueOf(value.charAt(value.length()-1));
                    if(last.equalsIgnoreCase("\n")){
                        value = value.replace("\n","");
                    }

                    cashWithdrawalaeps2RequestModel = new CashWithdrawalAEPS2RequestModel(amountEnter.getText().toString().trim(), uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer,value,"APIUSERAPI",session.getUserName());
                    cashWithdrawalPresenter = new CashWithdrawalPresenter(AEP2HomeActivity.this);
                    checkVPNstatusForTransactionCashWithdraw("Cash",aadharCardValidation);

                }

            }else{
                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }

    private class AuthRequestMorpho extends AsyncTask<Void, Void, String> {

        private String uid;
        private MorphoPidData morphoPidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        MorphoDeviceInfo morphoDeviceInfo ;

        private AuthRequestMorpho(String uid, MorphoPidData morphoPidData) {
            this.uid = uid;
            this.morphoPidData=morphoPidData;
            dialog = new ProgressDialog(AEP2HomeActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                morphoDeviceInfo = morphoPidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = morphoDeviceInfo.rdsId;
                meta.rdsVer = morphoDeviceInfo.rdsVer;
                meta.dpId = morphoDeviceInfo.dpId;
                meta.dc = morphoDeviceInfo.dc;
                meta.mi = morphoDeviceInfo.mi;
                meta.mc = morphoDeviceInfo.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = morphoPidData._Skey;
                authReq.Hmac = morphoPidData._Hmac;
                authReq.data = morphoPidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = session.getFreshnessFactor();
                //authReq.freshnessFactor = "";

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq!=null && meta!=null && morphoDeviceInfo!=null) {

                if (tag.equalsIgnoreCase("3")) {

                    String vid = null;
                    String uid = null;

                        uid = aadharNumber.getText().toString();
                    if(aadhaar.isEnabled()){
                        if (uid.contains(" ")) {
                            uid = uid.replaceAll(" ", "").trim();
                        }
                    }else if(virtualID.isEnabled()){
                        vid = aadharVirtualID.getText().toString().trim();
                        if (vid.contains(" ")) {
                            vid = vid.replaceAll(" ", "").trim();
                        }
                    }


                    balanceEnquiryaeps2RequestModel = new BalanceEnquiryAEPS2RequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString(),"WEBUSER",session.getUserName());
                    balanceEnquiryPresenter = new BalanceEnquiryPresenter(AEP2HomeActivity.this);
                    checkVPNstatusForTransaction("Balance");

                    //balanceEnquiryaeps2RequestModel = new BalanceEnquiryAEPS2RequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, balanceMobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString(),"MOBUSER",null);
                    // balanceEnquiryPresenter = new BalanceEnquiryPresenter(AEPS2HomeActivity.this);
                    //balanceEnquiryPresenter.performBalanceEnquiry(session.getUserName(),session.getUserToken(), balanceEnquiryRequestModel);
                    //  balanceEnquiryPresenter.performBalanceEnquiryAEPS2(session.getUserToken(), balanceEnquiryRequestModel);


                }
                else if (tag.equalsIgnoreCase("1")) {
                    String vid = null;
                    String uid = null;
                        uid = aadharNumber.getText().toString();
                    if(aadhaar.isEnabled()){
                        if (uid.contains(" ")) {
                            uid = uid.replaceAll(" ", "").trim();
                        }
                    }else if(virtualID.isEnabled()){
                        vid = aadharVirtualID.getText().toString().trim();
                        if (vid.contains(" ")) {
                            vid = vid.replaceAll(" ", "").trim();
                        }
                    }


                    cashWithdrawalaeps2RequestModel = new CashWithdrawalAEPS2RequestModel(amountEnter.getText().toString().trim(), uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer,authReq.skey.value.toString(),"WEBUSER",session.getUserName());//APIUSERAPI
                    cashWithdrawalPresenter = new CashWithdrawalPresenter(AEP2HomeActivity.this);
                    checkVPNstatusForTransactionCashWithdraw("Cash",aadharCardValidation);

                   /* cashWithdrawalRequestModel = new CashWithdrawalRequestModel(withdrawalAmountEnter.getText().toString().trim(), uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, withdrawalMobileNumber.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer, authReq.skey.value.toString(),AepsSdkConstants.paramA,AepsSdkConstants.paramB,AepsSdkConstants.paramC);
                    cashWithdrawalPresenter = new CashWithdrawalPresenter(AEPS2HomeActivity.this);
                    //cashWithdrawalPresenter.performCashWithdrawal(session.getUserName(),session.getUserToken(), cashWithdrawalRequestModel);
                    cashWithdrawalPresenter.performCashWithdrawalAEPS2(session.getUserToken(), cashWithdrawalRequestModel);
                   */
                }
                else if (tag.equalsIgnoreCase("2")) {
                  /*  String aadharNo = aadharNumber.getText ().toString ();
                    if (aadharNo.contains ( "-" )) {
                        aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
                    }
                    cashDepositRequestModel = new CashDepositRequestModel(amountEnter.getText().toString().trim(), aadharNo, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "DEPOSIT", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashDepositPresenter = new CashDepositPresenter(DashboardActivity.this);
                    cashDepositPresenter.performCashDeposit(session.getUserToken(), cashDepositRequestModel);*/
                }

            }else{
                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }
    /*
     * calendar data for the mantra and morpho
     *
     * capture date and time
     */
    private String generateTXN() {
        try {
            Date tempDate = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH);
            String dTTXN = formatter.format(tempDate);
            return dTTXN;
        } catch (Exception e) {
            return "";
        }
    }

    /*
     *  url for the sync of the data for the
     */

    private String getAuthURL(String UID) {
        String url = "http://developer.uidai.gov.in/auth/";
        url += "public/" + UID.charAt(0) + "/" + UID.charAt(1) + "/";
        url += "MG41KIrkk5moCkcO8w-2fc01-P7I5S-6X2-X7luVcDgZyOa2LXs3ELI"; //ASA
        return url;
    }

    private void checkBalanceEnquiryValidation() {
        // TODO Auto-generated method stub
        if (mobileNumber.getText () != null && !mobileNumber.getText ().toString ().trim ().matches ( "" )
                && Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == true && bankspinner.getText()!=null
                && !bankspinner.getText().toString().trim().matches("")) {

            boolean status = false;
                String aadharNo = aadharNumber.getText ().toString ();
                if (aadharNo.contains ( " " )) {aadharNo = aadharNo.replaceAll ( " ", "" ).trim ();
                status=true;
                }
//                status = Util.validateAadharNumber ( aadharNo );



            if (status) {
                if (deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));
                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }
                    if (!pidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));
                        submitButton.setEnabled ( false );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit_blue));
                    }
                } else if (deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )) {
                    if (morphoPidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));
                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }
                    if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));
                        submitButton.setEnabled ( false );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit_blue));
                    }
                }
            }
        }else {
            submitButton.setEnabled ( false );
            submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
        }

    }
    private void checkCashDepositValidation() {
        // TODO Auto-generated method stub
        if (mobileNumber.getText () != null && !mobileNumber.getText ().toString ().trim ().matches ( "" )
                && Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == true
                && mobileNumber.getText().toString().length () == 10
                && bankspinner.getText()!=null && !bankspinner.getText().toString().trim().matches("")
                && amountEnter.getText()!=null && !amountEnter.getText().toString().trim().matches("") ) {
            String aadharNo = aadharNumber.getText().toString();
            if (aadharNo.contains("-")) {
                aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
            }
            if (Util.validateAadharNumber (aadharNo) == true) {
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    } if (!pidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));
                        submitButton.setEnabled ( false );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit_blue));
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        submitButton.setEnabled ( false );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit_blue));
                    }
                }
            }
        }else{
            submitButton.setEnabled(false);
            submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
        }
    }
    private void checkWithdrawalValidation() {
        // TODO Auto-generated method stub
        if (mobileNumber.getText () != null
                && !mobileNumber.getText ().toString ().trim ().matches ( "" )
                && Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == true
                && mobileNumber.getText().toString().length () == 10
                && bankspinner.getText()!=null
                && !bankspinner.getText().toString().trim().matches("")
                && amountEnter.getText()!=null
                && !amountEnter.getText().toString().trim().matches("") ) {

            boolean status = false;
                String aadharNo = aadharNumber.getText ().toString ();
                if (aadharNo.contains ( " " )) {aadharNo = aadharNo.replaceAll ( " ", "" ).trim ();
                }
                status = Util.validateAadharNumber ( aadharNo );



            if (status) {
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }if (!pidData._Resp.errCode.equals ( "0" )) {
                        Log.v("panda","errorinfo"+pidData._Resp.errInfo);
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        submitButton.setEnabled ( false );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
                    } else {

                        submitButton.setEnabled ( true );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit_blue));
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        Toast.makeText ( AEP2HomeActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    } if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner));

                        submitButton.setEnabled ( false );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit_blue));
                    }
                }
            }
        }else {
            submitButton.setEnabled ( false );
            submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
        }

    }

    TextWatcher mWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub
            checkBalanceEnquiryValidation();

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
    }
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    public void releaseData() {
        amountEnter.setText(null);
        amountEnter.setError(null);
        aadharNumber.setText(null);
        aadharNumber.setError(null);

        mobileNumber.setText(null);
        mobileNumber.setError(null);

        bankspinner.setText(null);
        bankspinner.setError(null);


        bankIINNumber = "";



        tag = "";
        pidData=null;
        morphoPidData=null;
        balanceEnquiryaeps2RequestModel = null;
        cashWithdrawalaeps2RequestModel = null;

        depositBar.setVisibility ( View.GONE );
        depositNote.setVisibility ( View.GONE );
        fingerprintStrengthDeposit.setVisibility ( View.GONE );
    }





   /* private void checkUserDetails(){
        showLoader();
        String url = "https://apps.iserveu.online/get/result/"+AepsSdkConstants.paramA+"/"+AepsSdkConstants.paramB;

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //pd.dismiss();
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String msg = obj.getString("statusDesc");

                            if(status.equalsIgnoreCase("0")){
                                getUserAuthToken();
                               *//* if(session.getUserToken()!=null){
                                    hideLoader();
                                   *//**//* if (SDKConstants.transactionType.equalsIgnoreCase(SDKConstants.cashWithdrawal)){
                                        apiCalling();
                                    }  else if (SDKConstants.transactionType.equalsIgnoreCase(SDKConstants.balanceEnquiry)){
                                        balanceEnquiryApiCalling();
                                    }*//**//*
                                }else {
                                    getUserAuthToken();
                                }*//*
                            }else{
                                hideLoader();
                                showAlert(msg);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        showAlert("Invalid User");

                    }

                });

    }*/
    private void getUserAuthToken(){
        String url = AepsSdkConstants.BASE_URL+"/api/getAuthenticateData" ;
        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData",AepsSdkConstants.encryptedData);
            obj.put("retailerUserName",AepsSdkConstants.loginID);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if(status.equalsIgnoreCase("success")) {
                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");
                                    session.setUsername(userName);
                                    session.setUserToken(userToken);
                                    //session.setUserToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4ODM1OTk3NDEyLCJleHAiOjE1ODg4Mzc3OTd9.iKWhaqQFvm_fZ6F3otXMf5tSf6Dhe66mZKc9P-KKTWR7aBMuji0w9orCqJ8mdxBpj1DeZOYfrM5ExAMXggvzQw");
                                    hideLoader();
                                   /* if (SDKConstants.transactionType.equalsIgnoreCase(SDKConstants.cashWithdrawal)) {
                                        apiCalling();
                                    } else if (SDKConstants.transactionType.equalsIgnoreCase(SDKConstants.balanceEnquiry)) {
                                        balanceEnquiryApiCalling();
                                    }*/
                                }else {
                                    showAlert(status);
                                    hideLoader();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                showAlert("Invalid Encrypted Data");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();

                        }

                    });
        }catch ( Exception e){
            e.printStackTrace();
        }
    }

    private void checkVPNstatusForTransaction(final String transaction_type){
        // runProgressDialog();
        AndroidNetworking.get("https://vpn.iserveu.online/vpn/telnet_checkVpn")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // stopProgressDialog();
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if(status.equalsIgnoreCase("0")) {
                                if (transaction_type.equalsIgnoreCase("Cash")) {
                                    //check Velocity
                                    cashWithdrawalPresenter.performCashWithdrawalAEPS2(session.getUserToken(), cashWithdrawalaeps2RequestModel);
                                }


                                else if (transaction_type.equalsIgnoreCase("Balance")) {
                                    balanceEnquiryPresenter.performBalanceEnquiryAEPS2(session.getUserToken(), balanceEnquiryaeps2RequestModel,balanceEnquiryExpandButton.getText().toString());

                                } else {
                                    showUserOnboardStatus("Sorry, something went wrong. Please try after sometimes.");
                                }
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }
//check transaction

    private void checkVPNstatusForTransactionCashWithdraw(final String transaction_type,String checkCard){
        // runProgressDialog();
        AndroidNetworking.get("https://vpn.iserveu.online/vpn/telnet_checkVpn")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // stopProgressDialog();
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if(status.equalsIgnoreCase("0")) {
                                if (transaction_type.equalsIgnoreCase("Cash")) {
                                    //check Velocity
                                    ValidateAeps2Transaction(checkCard);
                                    //cashWithdrawalPresenter.performCashWithdrawalAEPS2(session.getUserToken(), cashWithdrawalaeps2RequestModel);
                                }


                                else if (transaction_type.equalsIgnoreCase("Balance")) {
                                    balanceEnquiryPresenter.performBalanceEnquiryAEPS2(session.getUserToken(), balanceEnquiryaeps2RequestModel,balanceEnquiryExpandButton.getText().toString());

                                } else {
                                    showUserOnboardStatus("Sorry, something went wrong. Please try after sometimes.");
                                }
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }




    public void ValidateAeps2Transaction(String cardAadhar) {

        String aadhar_sha = Util.getSha256Hash(cardAadhar);
        Constants.AADHAR_CARD = cardAadhar;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("retailerId",session.getUserName());
            jsonObject.put("aadhaar_card",aadhar_sha);
            jsonObject.put("amount",amountEnter.getText().toString().trim());

            AndroidNetworking.post("https://us-central1-creditapp-29bf2.cloudfunctions.net/isuApi/aeps/validate_aeps")
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusString = obj.getString("status");
                                if(statusString.equalsIgnoreCase("0")){
                                    String statusMsg = obj.getString("errorMessage");
                                    Util.showAlert(AEP2HomeActivity.this,"Alert",statusMsg);
                                }else {
                                    //Allow Transaction
                                    cashWithdrawalPresenter.performCashWithdrawalAEPS2(session.getUserToken(), cashWithdrawalaeps2RequestModel);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                Util.showAlert(AEP2HomeActivity.this,"Alert","Velocity api is not working due to some technical issue ..");


                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorBody();
                            Util.showAlert(AEP2HomeActivity.this,"Alert","Velocity api is not working due to some technical issue ..");

                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void showUserOnboardStatus(final String statusDesc){


        AlertDialog.Builder builder1 = new AlertDialog.Builder(AEP2HomeActivity.this);
        builder1.setMessage(statusDesc);
        builder1.setTitle("Alert");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public void showAlert(String msg){

        AlertDialog.Builder builder = new AlertDialog.Builder(AEP2HomeActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     *  BIO AUTH
     */
    public void showFingurePrintDialog(){
        /*LayoutInflater factory = LayoutInflater.from(AEPS2HomeActivity.this);
        final View deleteDialogView = factory.inflate(R.layout.dialog_fingure_info, null);
        deleteDialog = new AlertDialog.Builder(AEPS2HomeActivity.this).create();
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        deleteDialog.setView(deleteDialogView);
        deleteDialog.setCancelable(false);*/
        deleteDialog = new Dialog(AEP2HomeActivity.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteDialog.setCancelable(false);
        deleteDialog.setContentView(R.layout.dialog_fingure_info);
        two_fact_fingerprint = deleteDialog.findViewById(R.id.two_fact_fingerprint);
        two_fact_depositBar = deleteDialog.findViewById(R.id.progressBar);
        two_fact_submitButton = deleteDialog.findViewById(R.id.two_fact_submitButton);
        RadioButton bl_aadhar_no_rd = deleteDialog.findViewById(R.id.bl_aadhar_no_rd);
        RadioButton bl_aadhar_uid_rd =  deleteDialog.findViewById(R.id.bl_aadhar_uid_rd);
        EditText balanceAadharNumber = deleteDialog.findViewById(R.id.balanceAadharNumber);
        EditText balanceAadharVID = deleteDialog.findViewById(R.id.balanceAadharVID);

        balanceAadharNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    balanceAadharNumber.setError ( getResources ().getString ( R.string.aadhaarnumber ) );

                }

                if (s.length () > 0) {
                    balanceAadharNumber.setError ( null );
                    String aadharNo = balanceAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber (aadharNo) == false) {
                        balanceAadharNumber.setError ( getResources ().getString ( R.string.valid_aadhar_error ) );
                    }
                }
            }
        } );

        balanceAadharVID.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    balanceAadharVID.setError ( getResources ().getString ( R.string.aadhaarnumber_vid ) );

                }

                if (s.length () > 0) {
                    balanceAadharVID.setError ( null );
                    String aadharNo = balanceAadharVID.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID (aadharNo) == false) {
                        balanceAadharVID.setError ( getResources ().getString ( R.string.valid_aadhar__uid_error ) );
                    }
                }
            }
        } );

        two_fact_fingerprint.setOnClickListener ( new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                //showLoader ();


                if(bl_aadhar_no_rd.isChecked()) {
                    balanceaadharNo = balanceAadharNumber.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(balanceaadharNo) == false) {
                        balanceAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }

                if(bl_aadhar_uid_rd.isChecked()) {
                    balanceaadharNo = balanceAadharVID.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(balanceaadharNo) == false) {
                        balanceAadharVID.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                        return;
                    }
                }


               /* musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                updateDeviceList ();*/
                if(usbDevice !=null) {
                    if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( mantradeviceid )) {
                        //Toast.makeText ( DashboardActivity.this, "devicemantra"+usbDevice.getManufacturerName (), Toast.LENGTH_SHORT ).show ();
                        capture ();
                    } else if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphodeviceid )||usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphoe2device ) ) {
                        // Toast.makeText ( DashboardActivity.this, "devicemorpho"+usbDevice.getManufacturerName (), Toast.LENGTH_SHORT ).show ();
                        morophoCapture ();
                    }
                }else{
                    musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                    updateDeviceList ();
//                    deviceConnectMessgae ();
                }
            }
        } );
        two_fact_submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pidData!=null) {
                    new AuthRequestINDUS(balanceaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        deleteDialog.show();

    }


    private class AuthRequestINDUS extends AsyncTask<Void, Void, String> {

        private String uid,mobile;
        private PidData pidData;
        ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info ;

        private AuthRequestINDUS(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AEP2HomeActivity.this);
            dialog.setMessage("please wait..");
            dialog.setCancelable(false);
            dialog.show();

        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = "";

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if(dialog!=null){
                dialog.dismiss();
            }
            if (res != null && authReq!=null && meta!=null && info!=null) {


                String value = authReq.skey.value.toString();

                //  String last = value.charAt((value.length()) -1);
                String last = String.valueOf(value.charAt(value.length()-1));
                if(last.equalsIgnoreCase("\n")){
                    value = value.replace("\n","");
                }
                JSONObject obj = new JSONObject();
                try {
                    obj.put("aadharNo", uid);
                    obj.put("dpId", meta.dpId);
                    obj.put("rdsId", meta.rdsId);
                    obj.put("rdsVer", meta.rdsVer);
                    obj.put("dc", meta.dc);
                    obj.put("mi", meta.mi);
                    obj.put("mcData",  meta.mc);
                    obj.put("sKey", value);
                    obj.put("hMac", authReq.Hmac);
                    obj.put("encryptedPID", authReq.data.value);
                    obj.put("ci", authReq.skey.ci);
                    obj.put("operation","");
                    submitBioAuthh(mobile, uid, obj);
                }catch (Exception e){
                    e.printStackTrace();
                }
                // balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, balanceMobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, value);
            }else{
                Util.showAlert(AEP2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }
    private void submitBioAuthh(final String mobile, final String aadhar, JSONObject obj1){
        showLoader();
        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v80")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            submitBioAuth(mobile,aadhar,obj1,encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }




    private void submitBioAuth(final String mobile, final String aadhar, JSONObject obj,final String encodedUrl){
        // showLoader();
        AndroidNetworking.post(encodedUrl)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", session.getUserToken())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();

                        try {
                            // profile_id,is_declaration
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            if(status.equalsIgnoreCase("0")) {
                                deleteDialog.dismiss();
                                AepsSdkConstants.bioauth=false;
                                Toast.makeText(AEP2HomeActivity.this, "SUCCESS", Toast.LENGTH_SHORT).show();

                            }else{
                                Toast.makeText(AEP2HomeActivity.this, "FAILURE", Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AEP2HomeActivity.this, "FAILURE", Toast.LENGTH_SHORT).show();

                    }
                });
    }


}

